package config

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

type AppProps struct {
	AppPort int
	DB DB
}

type DB struct {
	Host string
	Port int
	Name string
	User string
	Pass string
}

var Props *AppProps
var ProjectRoot = getRootProjectDir()

func init() {
	initProps()
	initLogs()
}

func initProps() {
	viper.SetConfigName("config")
	viper.AddConfigPath(ProjectRoot)
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.SetEnvPrefix("glofox")
	viper.AutomaticEnv()

	err := viper.Unmarshal(&Props)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
}

func initLogs() {
	log.SetOutput(os.Stdout)
	log.SetReportCaller(true)
}

func getRootProjectDir() string {
	_, b, _, _ := runtime.Caller(0)
	return filepath.Dir(filepath.Dir(b))
}
