
	docker run  -e POSTGRES_PASSWORD=postgres -d postgres

docker_db_name = glofox-postgres-45373453

run:
	@echo "Run 'make clean' after... to delete postgres instance"
	@[ ! "$$(docker ps | grep $(docker_db_name) )" ] && docker run -d --name $(docker_db_name) -p 8384:5432 -e POSTGRES_PASSWORD=123456 -d postgres || true
	@export GLOFOX_DB_PASS=123456 GLOFOX_DB_PORT=8384 && go run main.go
clean:
	@docker stop $(docker_db_name) && docker rm $(docker_db_name) || true

