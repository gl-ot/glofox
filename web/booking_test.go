package web

import (
	"bitbucket.com/gl-ot/glofox/domain"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/magiconair/properties/assert"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

func TestCreateBooking(t *testing.T) {
	now := time.Now()
	postBooking := domain.Booking{Name: "John", Date: now}
	jsonStr, err := json.Marshal(postBooking)
	if err != nil {
		t.Error(err)
	}
	resp, err := http.Post(fmt.Sprintf("%s/bookings", ts.URL), "application/json", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Error("Failed post: ", err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	var booking domain.Booking
	err = json.Unmarshal(body, &booking)
	if err != nil {
		t.Error(err)
	}

	if booking.ID == 0 {
		t.Errorf("Booking id is not set: expecting positive number, got %d", booking.ID)
	}

	assert.Equal(t, booking.Name, postBooking.Name)
	assert.Equal(t, booking.Date.Format(dateLayoutIso), postBooking.Date.Format(dateLayoutIso))
}
