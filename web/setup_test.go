package web

import (
	"bitbucket.com/gl-ot/glofox/config"
	"bitbucket.com/gl-ot/glofox/db"
	"database/sql"
	"fmt"
	"github.com/ory/dockertest/v3"
	"log"
	"net/http/httptest"
	"os"
	"strconv"
	"testing"
)

const (
	databaseName = "postgres"
	dateLayoutIso = "2006-04-15T03:34:34Z"
)

var database *sql.DB
var ts *httptest.Server

func TestMain(m *testing.M) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	resource, err := pool.Run("postgres", "9.6", []string{"POSTGRES_PASSWORD=secret", "POSTGRES_DB=" + databaseName})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	var port int
	if err = pool.Retry(func() error {
		var err error
		portStr := resource.GetPort("5432/tcp")
		port, err = strconv.Atoi(portStr)
		if err != nil {
			log.Fatal(err)
		}
		database, err = sql.Open("postgres", fmt.Sprintf("postgres://postgres:secret@localhost:%s/%s?sslmode=disable", portStr, databaseName))
		if err != nil {
			return err
		}
		return database.Ping()
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	config.Props.DB.Port = port
	db.InitDB()

	ts = httptest.NewServer(SetupRouter())
	defer ts.Close()

	code := m.Run()

	// You can't defer this because os.Exit doesn't care for defer
	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}
