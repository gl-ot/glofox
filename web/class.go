package web

import (
	"bitbucket.com/gl-ot/glofox/domain"
	"bitbucket.com/gl-ot/glofox/service"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func CreateClass(c *gin.Context) {
	var input domain.Class
	if err := c.ShouldBindJSON(&input); err != nil {
		log.Debug(err)
		c.JSON(http.StatusBadRequest, err)
		return
	}

	class, err := service.CreateClass(input)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, class)
}
