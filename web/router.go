package web

import (
	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()
	r.POST("/classes", CreateClass)
	r.POST("/bookings", CreateBooking)
	return r
}
