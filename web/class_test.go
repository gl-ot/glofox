package web

import (
	"bitbucket.com/gl-ot/glofox/domain"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

func TestCreateClass(t *testing.T) {
	now := time.Now()
	postClass := domain.Class{Name: "Pilates", StartDay: now, EndDay: now.AddDate(0, 0, 1), Capacity: 10}
	jsonStr, err := json.Marshal(postClass)
	if err != nil {
		t.Error(err)
	}
	resp, err := http.Post(fmt.Sprintf("%s/classes", ts.URL), "application/json", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Error("Failed post: ", err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	var class domain.Class
	err = json.Unmarshal(body, &class)
	if err != nil {
		t.Error(err)
	}

	if class.ID == 0 {
		t.Errorf("Booking id is not set: expecting positive number, got %d", class.ID)
	}

	assert.Equal(t, postClass.Name, class.Name)
	assert.Equal(t, postClass.StartDay.Format(dateLayoutIso), class.StartDay.Format(dateLayoutIso))
	assert.Equal(t, postClass.EndDay.Format(dateLayoutIso), class.EndDay.Format(dateLayoutIso))
	assert.Equal(t, postClass.Capacity, class.Capacity)
}
