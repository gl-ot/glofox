package main

import (
	"bitbucket.com/gl-ot/glofox/config"
	"bitbucket.com/gl-ot/glofox/db"
	"bitbucket.com/gl-ot/glofox/web"
	"fmt"
	log "github.com/sirupsen/logrus"
)

func main() {
	db.InitDB()
	defer db.DB.Close()

	r := web.SetupRouter()
	log.Infof("Starting web server on %d port", config.Props.AppPort)
	r.Run(fmt.Sprintf(":%d", config.Props.AppPort))
}
