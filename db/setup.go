package db

import (
	"bitbucket.com/gl-ot/glofox/config"
	"bitbucket.com/gl-ot/glofox/domain"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
)

var DB *gorm.DB

func InitDB() {
	db, err := gorm.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=disable",
		config.Props.DB.Host, config.Props.DB.Port, config.Props.DB.User, config.Props.DB.Name, config.Props.DB.Pass))
	if err != nil {
		log.Fatal("Failed to connect to database: ", err)
	}

	db.AutoMigrate(&domain.Class{})
	db.AutoMigrate(&domain.Booking{})

	DB = db
}