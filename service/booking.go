package service

import (
	"bitbucket.com/gl-ot/glofox/db"
	"bitbucket.com/gl-ot/glofox/domain"
)

func CreateBooking(booking domain.Booking) (domain.Booking, error) {
	booking.ID = 0
	db.DB.Save(&booking)
	return booking, nil
}
