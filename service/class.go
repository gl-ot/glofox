package service

import (
	"bitbucket.com/gl-ot/glofox/db"
	"bitbucket.com/gl-ot/glofox/domain"
)

func CreateClass(class domain.Class) (domain.Class, error) {
	class.ID = 0
	db.DB.Create(&class)
	return class, nil
}
