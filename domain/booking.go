package domain

import "time"

type Booking struct {
	ID uint    `json:"id" gorm:"primary_key"`
	Name string `json:"name"` // the name of the class
	Date time.Time `json:"date"`
	// It should have at least these fields:
	// ClassID (name is not unique)
	// UserID  (the one who booked up the class)
}
