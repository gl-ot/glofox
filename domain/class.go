package domain

import "time"

type Class struct {
	ID       uint    `json:"id" gorm:"primary_key"`
	Name     string `json:"name"`
	StartDay time.Time `json:"start_day"`
	EndDay   time.Time `json:"end_day"`
	Capacity uint    `json:"capacity"`
	// Probably there's going to be this fields:
	// NetworkID,
	// Location,
	// InstructorID
	// Description
	// StartDay, EndDay obviously is not enough.
	//   They should be at-least in one-to-many relationship.
	//   And a better solution is to have some kind of Repeater in case of eternal course
}
